var gulp = require("gulp");
var es = require("event-stream");
var clean = require("gulp-clean");
var coffee = require("gulp-coffee");
var gutil = require("gulp-util");
var connect = require("gulp-connect");
var path = require("path");
var less = require("gulp-less");
var concat = require("gulp-concat");


function errorHandler(error) {
  console.error("Oops something went wrong:", error);
  this.emit("end");
}


gulp.task("default", ["connect", "watch"]);

gulp.task("compile", ["coffee", "less"]);

gulp.task("coffee", ["cleanBuild"], function() {

  console.log("Compiling coffee sources...");

  return gulp.src("./src/coffee/**/*.coffee")
    .pipe(concat("app.js"))
    .pipe(coffee().on("error", errorHandler))
    .pipe(gulp.dest("./build/scripts"));
});

gulp.task("less", ["cleanBuild"], function () {

  console.log("Compiling less sources...");

  return gulp.src("./src/less/**/*.less")
    .pipe(less({
      paths: [ path.join(__dirname, "less", "includes") ]
    })).on("error", errorHandler).pipe(gulp.dest("./build/styles"));

});

gulp.task("copyHTML", ["cleanBuild"], function() {

  console.log("Copying HTML files...")

  gulp.src("./src/html/**/*.html")
    .pipe(gulp.dest("./build")).pipe(connect.reload());
})

gulp.task("copyLibs", ["cleanBuild"], function() {

  console.log("Copying libs into build folder...");

  es.merge(
    gulp.src("./bower_components/angular/angular.js"),
    gulp.src("./bower_components/angular-route/angular-route.js"),
    gulp.src("./bower_components/bootstrap/dist/css/bootstrap.css"),
    gulp.src("./bower_components/underscore/underscore.js")
  ).pipe(gulp.dest("./build/libs")).on("error", errorHandler);
})

gulp.task("copyFonts", ["cleanBuild"], function() {

  console.log("Copying fonts into build folder...");

  gulp.src("./bower_components/bootstrap/fonts/*.*")
    .pipe(gulp.dest("./build/fonts")).on("error", errorHandler);
})



gulp.task("copyAssets", ["cleanBuild"], function() {

  console.log("Copying assets...")

  gulp.src("./src/assets/**/*.*")
    .pipe(gulp.dest("./build"));
})


gulp.task("cleanBuild", function() {

  console.log("Cleaning build folder...");

  return gulp.src("./build", {read:false}).pipe(clean());
})

gulp.task("build", ["compile", "copyHTML", "copyLibs", "copyAssets", "copyFonts"], function() {
  // console.log("\n\nRunning new build...\n");
})



gulp.task("watch", function() {
  gulp.watch("./src/**/*", ["build"])
})


gulp.task("connect", function() {
  connect.server({
    root: "build",
    port: 8000,
    livereload: false
  });
});
