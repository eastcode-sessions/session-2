myapp = angular.module("eastcodeSessions", [
  "ngRoute"
])

myapp.config([
  "$routeProvider"
  ($routeProvider) ->

    # Routing Configuration
    $routeProvider.when("/",
      {
        templateUrl: "partials/overview.html"
        controller: "overviewController"
      }
    ).when("/sessions/:sessionId/register",
      {
        templateUrl: "partials/register.html"
        controller: "registerController"
      }
    ).when("/sessions/:sessionId/attendees",
      {
        templateUrl: "partials/attendees.html"
        controller: "attendeesController"
      }
    ).otherwise(
      {
        redirectTo: "/"
      }
    )
])
