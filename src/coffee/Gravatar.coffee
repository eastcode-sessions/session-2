#_require ../Module

myapp.directive("gravatar", [() ->

  link = ($scope, element, attrs, controller) ->

    calculateHash = (email) ->
      CryptoJS.MD5(email.toLowerCase())

    getImageURL = (email) ->
      hash = calculateHash(email)
      "http://www.gravatar.com/avatar/#{hash}.jpg"



    $scope.$watch("email", (newValue, oldValue) ->
      if newValue
        $scope.imageURL = getImageURL(newValue)
    )

  {
    link: link
    restrict: "A"
    template: "<img ng-src='{{imageURL}}'></img>"
    scope:
      email: "="
  }
])
