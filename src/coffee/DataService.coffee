class DataService

  @DummyNames = ["Richard", "Tami", "Golden", "Darleen", "Darius", "Racheal", "Kelli",
    "Jean", "Genevieve", "Randa", "Gregorio", "Arnold", "Laurene", "Bunny", "Dwain", "Mazie",
    "Theda", "Golda", "Anna", "Josephine", "Buster", "Delmer", "Ernestine",
    "Angelia", "Cassi", "Freida", "Jannie", "Tina", "Caron", "Leona", "Betsy", "Joaquina",
    "Marivel", "Lise", "Magaly", "Daniella", "Valorie", "Isaiah", "Romana", "Fritz",
    "Cinthia", "Mafalda", "Darryl", "Latasha", "Joaquin", "Gracie", "Boyce", "Elda",
    "Harry", "Laurice" ]

  @getRandomAttendees = () ->
    numberOfAttendees = 5 + Math.floor(Math.random()*10)
    attendees = []

    for i in [1..numberOfAttendees]
      name = @DummyNames[Math.floor(Math.random()*49)]

      attendees.push({
        name: name
        email: "#{name.toLowerCase()}@email.com"
      })

    attendees


  constructor: (@q, @timeout) ->

    @sessions = [
      {
        id: 1
        title: "Angular JS"
        date: new Date(2014, 6, 18, 19, 0, 0)
        attendees: DataService.getRandomAttendees()
      }
      {
        id: 2
        title: "Elasticsearch"
        date: new Date(2014, 8, 17, 19, 0, 0)
        attendees: DataService.getRandomAttendees()
      }
      {
        id: 3
        title: "Vert.x"
        date: new Date(2014, 7, 16, 19, 0, 0)
        attendees: DataService.getRandomAttendees()
      }
    ]


  delay: (time, data) ->
    result = @q.defer()

    @timeout(() ->
      if data
        result.resolve(data)
      else
        result.reject("Data not found!")
    , time)

    result.promise

  getSessions: () -> @delay(600, @sessions)

  getSession: (sessionId) ->
    @delay(400, _.find(@sessions, (session) ->
      session.id+"" == sessionId
    ))

  findAttendeeByEmail: (sessionId, email) ->

    @getSession(sessionId).then((session) =>

      result = @q.defer()

      attendee = _.find(session.attendees, (attendee) ->
        attendee.email.toLowerCase() == email
      )

      if attendee
        result.resolve(attendee)
      else
        result.reject("Attendee not found!")

      result.promise
    )

  register: (sessionId, attendee) ->
    @getSession(sessionId).then((session) ->
      session.attendees.push(attendee)
      attendee
    )

myapp.service("dataService", [
  "$q"
  "$timeout"
  DataService
])
