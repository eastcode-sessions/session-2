myapp.controller("registerController", [
  "$scope",
  "$routeParams",
  "$location",
  "$q",
  "dataService",
  ($scope, $routeParams, $location, $q, dataService) ->

    $scope.attendee = {}

    $scope.goToOverview = () ->
      $location.path("/")

    $scope.loadSession = (sessionId) ->

      $scope.loading = true

      dataService.getSession(sessionId).then((session) ->
        $scope.session = session
        $scope.loading = false
      ,(error) ->
        console.error(error)
      )

    # Register function will try to register
    # attendee with provided data.

    $scope.register = () ->

      $scope.validateInput($scope.attendee).then(() ->
        $scope.error = null
        dataService.register($routeParams.sessionId, $scope.attendee).then((attendee) ->
          console.log("Successfully registered", attendee)
          $scope.goToOverview()
        ,(error) ->
          $scope.error = error
        )
      ,(error) ->
        console.log(error)
        $scope.error = error
      )


    # This function will validate attendee data
    # and return promise which is rejected when
    # validation failed and resolved when everything
    # looks ok.

    $scope.validateInput = (attendee) ->

      result = $q.defer()

      if not attendee.name
        result.reject("Name must be defined!")
      else if attendee.name and attendee.name.length < 3
        result.reject("Name must be longer than 3 characters!")
      else if not attendee.email
        result.reject("Email must be defined!")
      else
        result.resolve(dataService.findAttendeeByEmail($routeParams.sessionId, attendee.email).then((attendee) ->
          $q.reject("Attendee with the email address already registered!")
        ,() ->
          $q.when("OK")
        ))

      result.promise


    # Let's validate attendee object when it gets modified

    $scope.$watch("attendee", (newValue, oldValue) ->
      $scope.validateInput(newValue).then(() ->
        console.log("Input fine")
        $scope.error = null
      ,(error) ->
        $scope.error = error
      )
    , true)

])
