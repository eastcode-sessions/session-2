myapp.controller("attendeesController", [
  "$scope",
  "$routeParams",
  "$location",
  "dataService",
  ($scope, $routeParams, $location, dataService) ->

    $scope.goToOverview = () ->
      $location.path("/")

    $scope.loadSession = (sessionId) ->

      $scope.loading = true
      $scope.error = false

      dataService.getSession(sessionId).then((session) ->
        $scope.session = session
        $scope.loading = false
      ,(error) ->
        console.error(error)
        $scope.error = true
      )

    $scope.loadSession($routeParams.sessionId)

])
