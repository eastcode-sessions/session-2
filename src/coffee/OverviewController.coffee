myapp.controller("overviewController", [
  "$scope",
  "$location",
  "dataService",
  ($scope, $location, dataService) ->

    $scope.loadSessions = () ->
      $scope.loading = true

      dataService.getSessions().then((sessions) ->
        $scope.sessions = sessions
        $scope.loading = false
      )

    $scope.seeAttendeesList = (sessionId) ->
      $location.path("sessions/#{sessionId}/attendees")

    $scope.register = (sessionId) ->
      $location.path("sessions/#{sessionId}/register")


    # Let's load sessions
    $scope.loadSessions()

])
